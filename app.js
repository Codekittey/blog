/**
 * Created by makraus on 31/07/2015.
 */
var express = require('express');
var app = express();

app.get('/', function (req, res) {
    res.sendfile('./public/index.html');
});

 app.use(express.static(__dirname + '/public'));

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});